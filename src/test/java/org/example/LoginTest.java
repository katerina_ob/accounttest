package org.example;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;

public class LoginTest {
    public static LoginPage loginPage;
    public static ProfilePage profilePage;
    public static WebDriver driver;

    @BeforeAll //@BeforeClass
    public static void setup() {
        //opredelenie puti do draivera i ego nastroika
        //System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        System.setProperty("webdriver.chrome.driver", ConfProperties.getProperty("chromedriver"));
        //sozdanie ekzempleara draivera
        //WebDriver driver = new ChromeDriver();
        driver = new ChromeDriver();
        loginPage = new LoginPage(driver);
        profilePage = new ProfilePage(driver);
        //okno razvoracivaetsea na polnii ekran
        driver.manage().window().maximize();
        //zaderjka vipolnenia testa, jdeom zagrizki elementa
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //polucenie ssilki na stranitsy vhoda iz faila nastroek
        //driver.get("https://passport.yandex.ru/auth");
        driver.get(ConfProperties.getProperty("loginpage"));
    }

    //тестовый метод для осуществления аутентификации
    @Test
    public void loginTest(){
        //получение доступа к методам класса LoginPage для взаимодействия с элементами страницы
        //значение login/password берутся из файла настроек по аналогии с chromedriver и loginpage
        //вводим логин
        loginPage.inputLogin(ConfProperties.getProperty("login"));
        //нажимаем кнопку входа
        loginPage.clickLoginBtn();
        //вводим пароль
        loginPage.inputPasswd(ConfProperties.getProperty("password"));
        //нажимаем кнопку входа
        loginPage.clickLoginBtn();
        //получаем отображаемый логин
        String user = profilePage.getUserName();
        //и сравниваем его с логином из файла настроек
        //Assert -> Assertions
        Assertions.assertEquals(ConfProperties.getProperty("login"), user);
    }

    //осуществление выхода из аккаунта с последующим закрытием окна браузера
    @AfterAll
    public static void tearDown() {
        profilePage.entryMenu();
        profilePage.userLogout();
        driver.quit();
    }

}
